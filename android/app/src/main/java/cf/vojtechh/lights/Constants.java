package cf.vojtechh.lights;

final class Constants {

    private  Constants() {

    }

    static final String Token = "9fc96c26f3e38e6af0b6fffc2e53b6d3";
    static final String AllowedSSID = "mobos";
    static final String AddressPreference = "device_address";
    static final String DefaultDeviceAddress = "10.10.10.10";
    static final int DevicePort = 2148;
    static final String PreferencesName = "Preferences";

}
